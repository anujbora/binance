import binance

api_key = ""
secret_key = ""

binance.set(api_key, secret_key)

ethPrice = 850
btcPrice = 9000

# BTC
binance.order("TRACBTC", binance.SELL, 1000, format((1.5 / btcPrice), '.7f')) # $1.5

# ETH
binance.order("TRACETH", binance.SELL, 1000, format((1.5 / ethPrice), '.6f')) # $1.5
